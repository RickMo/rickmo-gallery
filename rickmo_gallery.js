!function ($) {

   "use strict"; // jshint ;_;

   /* RICKMO GALLERY DEFINITION
    * ========================= */

   var RickMoGallery = function (element, options) {
       this.$element              = $(element)
       this.options               = options
       this.$pictureContainer     = null
       this.$picture              = null
       this.$thumbsContainer      = null
       this.selectedThumb         = 0
       this.previousSelectedThumb = 0
       this.usedPicture           = null
       this.$inhibitor            = null
       this.isLoadingProccess     = true
       this.thumbnails            = {}
       this.initialize()
   }

   RickMoGallery.prototype = {
     constructor: RickMoGallery

  ,  initialize: function () {
       this.pictureContainerSetup()
       this.thumbsContainerSetup()
       this.inhibitorSetup()
       this.selectThumb(0, true)
       this.loadPaginationControls()
     }

  ,  pictureContainerSetup: function () {
        var myGallery = this
        this.$pictureContainer = this.$element.children('.picture_container')
        this.$picture = this.$pictureContainer.children('img:first-child')
        this.$pictureBack = this.$picture.clone()
        this.$picture.toggle(false)
        this.$pictureBack.toggle(false)
        this.$picture.on('load', function () {
            myGallery.togglePictures(myGallery.$picture, myGallery.$pictureBack)
        })
        this.$pictureBack.on('load', function () {
            myGallery.togglePictures(myGallery.$pictureBack, myGallery.$picture)
        })
        this.$pictureBack.appendTo(this.$pictureContainer)
        this.setLoadingPosition(true)
        this.$picture.attr('src', this.options['large_dir'] + '/' + this.options['thumbs_files'][0])
        this.usedPicture = myGallery.$picture
        if (this.options['heightBindingControl'] != undefined) {
            this.options['heightBindingControl'].on('click', function () {
                myGallery.updateContainerPictureHeight()
            })
        }
     }

  ,  togglePictures: function ($controlOn, $controlOff) {
        $controlOff.fadeOut()
        $controlOn.fadeIn()
        this.usedPicture = $controlOn
        this.$pictureContainer.height($controlOn.height())
        this.$inhibitor.css('top', "-9999px");

        this.setLoadingPosition(false)
        this.isLoadingProccess = false
     }

  ,  updateContainerPictureHeight: function () {
        this.$pictureContainer.height(this.usedPicture.height());
     }

  ,  thumbsContainerSetup: function () {
        this.options.thumb_html_template ='<li class="span' + this.options.thumb_span_size + '"><a href="#" class="thumbnail"><img src="" alt="" /></a</li>'
        /* reference to hack context in closure */
        var $thumbsListContainer = this.$element.children('.thumbnails_set_container')
        this.$thumbsContainer = $thumbsListContainer.children('ul:first')
        this.thumbsInitialize()
        this.loadThumbsForPage(1)
     }

  ,  inhibitorSetup: function () {
        this.$inhibitor = $('<div id="inhibitor" style="top:-1000px; position:absolute; width:100%; height:100%"></div>').appendTo(this.$thumbsContainer.parent())
     }

   , loadThumbsForPage: function (pageNumber) {
        var myGallery = this
          , init  = (pageNumber - 1) * this.thumbnails['per_page']
          , limit = init + this.thumbsNumberForPage(pageNumber)

        // i have to delete all the children previously created
        this.$thumbsContainer.empty()

        for (var i=init; i<limit; ++i) {
            var tmpThumbData = { 'path': this.getThumbPath(i), 'alt': "" }
            var $tmpElement = $($(this.getThumbInstanceTemplate(tmpThumbData, i)));
            $tmpElement.children().on('click', { index: i }, function (event) {
                if (myGallery.isLoadingProccess === false) {
                    myGallery.$inhibitor.css('top', 0)
                    myGallery.isLoadingProccess = true
                    myGallery.setLoadingPosition(true)
                    event.preventDefault()
                    myGallery.setImageAndThumb(event)
                }
            })
            this.$thumbsContainer.append($tmpElement)
        }
     }

   , setLoadingPosition: function (visible) {
       if (visible) {
           var marginTop = this.$pictureContainer.position().top;
           var initTop   = Math.floor(this.$pictureContainer.height() / 2) - 32
           initTop = initTop > 0 ? initTop : 32
           marginTop = marginTop > 0 ? marginTop : 150
           var left = (Math.floor(this.$pictureContainer.width() / 2)  - 32).toString() + "px"
           var top  = (marginTop + initTop).toString() + "px"
           var cssProps = {
             'left': left,
             'top' : top,
             'position': 'absolute'
           }

           $('.loading').css(cssProps)
       } else {
           $('.loading').css('top', "-9999px")
       }
     }

   , thumbsNumberForPage: function (pageNumber) {
        if (pageNumber <= this.thumbnails['pages_number']) {
           var prevThumbsNumber = this.thumbnails['per_page'] * (pageNumber - 1)
           var remainingThubmsNumber = this.thumbnails['total'] - prevThumbsNumber

           return Math.min(this.thumbnails['per_page'], remainingThubmsNumber)
        }
        return -1
     }

   , getThumbPath: function (index) {
        return this.options['thumbs_dir'] + '/' + this.options['thumbs_files'][index]
     }

   , getThumbInstanceTemplate: function (thumbOptions, index) {
        var tmpTemplate = this.options['thumb_html_template']
        tmpTemplate = tmpTemplate.replace('src=""', 'src="' + thumbOptions['path'] + '"')
        tmpTemplate = tmpTemplate.replace('alt=""', 'alt="' + thumbOptions['alt'] + '"')
        if ( (index + 1) % this.thumbnails['per_row']  == 1) {
            tmpTemplate = tmpTemplate.replace('<li class="', '<li class="thumbrow_clear ')
        }
        return tmpTemplate
     }

   , setImageAndThumb: function (event) {
        var $inObject = this.selectThumb(event.data.index, true)
        this.selectThumb(this.previousSelectedThumb, false)
        this.setImage(event.data.index, $inObject)
     }

   , selectThumb: function (index, select) {
        var liThumb = this.$thumbsContainer.children().eq(index % this.thumbnails['per_page'])
        if (select) {
            var $aContainer = $(liThumb).children('a:first').addClass('selected')
            this.previousSelectedThumb = this.selectedThumb
            this.selectedThumb = index
        } else {
            $(liThumb).children('a:first').removeClass('selected')
        }
     }

   , setImage : function (imgIndex) {
       var imageFile = this.options['large_dir'] + '/' + this.options['thumbs_files'][imgIndex]

       // ...If right now i'm using this.$picture i have to use the other picture control and viceversa
       if (this.usedPicture[0] === this.$picture[0]) {
         this.$pictureBack.attr('src', imageFile)
       } else {
         this.$picture.attr('src', imageFile)
       }
     }

   , thumbsInitialize: function () {
        this.thumbnails['total'] = this.options['thumbs_files'].length
        this.thumbnails['per_row'] = Math.floor(this.options['thumbnails_container_span_width'] /
                                                this.options['thumb_span_size'])
        if (this.thumbnails['per_row'] == 0) this.thumbnails['per_row'] =1
        this.thumbnails['per_page'] = this.thumbnails['per_row'] * this.options['thumbs_rows']
        this.thumbnails['pages_number'] = this.calculateThumbsPagesNumber()
        this.thumbnails['current_page'] = 1
        this.thumbnails['prev_page'] = 1
        this.thumbnails['pagination'] = this.thumbnails['pages_number'] > 1
        this.thumbnails['prev_button_text']   = ''
        this.thumbnails['prev_button_title']  = 'Prev'
        this.thumbnails['next_button_text']   = ''
        this.thumbnails['next_button_title']  = 'Next'
        this.thumbnails['pagination_control'] = null
     }

   , calculateThumbsPagesNumber: function () {
        var res = Math.floor(this.thumbnails['total'] / this.thumbnails['per_page'])
        if (this.options['thumbs_files'].length % this.thumbnails['per_page'] != 0) res += 1

        return res
     }

   , loadPaginationControls: function () {
       var myGallery = this
       if (this.thumbnails['pages_number'] > 1) {
         var $thumbsListContainer = this.$element.children('.thumbnails_set_container')
         var $div = $('<div/>').addClass("pagination").appendTo($thumbsListContainer)
         var $ul = $('<ul/>').appendTo($div)
         this.thumbnails['pagination_control'] = $ul
         var $prev = $('<li class="disabled"><a href="#" title="' + this.thumbnails['prev_button_title'] + '">' +
                       this.thumbnails['prev_button_text'] +
                       '</a></li>').appendTo($ul)
         $('<li class="active"><a href="#">1</a></li>').appendTo($ul)
         for (var i=2; i<= this.thumbnails['pages_number']; ++i) {
            $('<li><a href="#">' + i + '</a></li>').appendTo($ul)
         }
         $prev = $('<li><a href="#" title="' + this.thumbnails['next_button_title']+ '">' + this.thumbnails['next_button_text'] + '</a></li>').appendTo($ul)
         $ul.children().each(function () {
             $(this).children().on('click', function (event) {
                 event.preventDefault()
                 if (myGallery.isLinkEnabled(this))
                    myGallery.setupThumbListRequest($(this).text() === "" ? $(this).attr('title') : $(this).text())
             })
         })
       }
     }
   , isLinkEnabled: function (aControl) {
        // a Control (in twitter bootstrap pagination) is within li element
        return (! $(aControl).parent().hasClass('disabled'))
     }

   , setupThumbListRequest: function (index) {
       index = this.checkPrevNextIndex(index)
       this.loadThumbsForPage(index)
       this.thumbnails['prev_page'] = this.thumbnails['current_page']
       this.thumbnails['current_page'] = index;
       this.updatePaginationControls(index)
     }

   , updatePaginationControls: function (index) {
       var $thumbsContainer = this.thumbnails['pagination_control']
         , $prev = $thumbsContainer.children('li:first')
         , $next = $thumbsContainer.children('li:last')

       $prev.removeClass('disabled')
       $next.removeClass('disabled')

       if (index == this.thumbnails['pages_number']) {
         $next.addClass('disabled')
       }

       if (index == 1) {
         $prev.addClass('disabled')
       }

       $thumbsContainer.children('li:eq(' + this.thumbnails['prev_page'] + ')').removeClass('active')
       $thumbsContainer.children('li:eq(' + this.thumbnails['current_page'] + ')').addClass('active')
     }

   , checkPrevNextIndex: function (index) {
       if ( index == this.thumbnails['prev_button_title'] ) {
           index = parseInt(this.thumbnails['current_page']) - 1
       } else if (index == this.thumbnails['next_button_title']) {
           index = parseInt(this.thumbnails['current_page']) + 1
       }

       return index
     }

   }

  /* RICKMO GALLERY PLUGIN DEFINITION
   * ================================ */

   $.fn.rickmoGallery = function (options) {
        return this.each(function () {
            var $this = $(this)
              , data = $this.data('rickmoGallery')
              , settings = $.extend($.fn.rickmoGallery.defaults, options)

            if (!data) $this.data('rickmoGallery', (data = new RickMoGallery(this, settings)))
        })
   }

   $.fn.rickmoGallery.defaults = {
     thumbnails_container_span_width : 4
   , picture_container_span_width    : 'span8'
   , thumbs_dir                      : 'img/small'
   , large_dir                       : 'img/large'
   , thumbs_files                    : new Array()
   , thumbs_rows                     : 2
   , thumb_span_size                 : 1
   , thumb_html_template             : '<li class="span1"><a href="#" class="thumbnail"><img src="" alt="" /></a</li>'
   }



}(window.jQuery);


/** TODO:
 *
 * Método initialize
 * -----------------
 * comprobar que el array tenga valores, que llegue el picture_container y que tenga una imagen
 *
 * Método getThumbPath
 * -------------------
 * Comprobar que está dentro de los límites y que están definidos los arrays
 *
 * SelectThumb
 * -----------
 * Creo que sería bueno hacer un método que vea los límites. Así vale para los anteriores también
 *
 *
 **/
